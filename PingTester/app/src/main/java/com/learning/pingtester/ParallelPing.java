package com.learning.pingtester;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ParallelPing {

    @TargetApi(Build.VERSION_CODES.N)
    public static List<PingResult> PingServers(List<String> MainList)
    {
        int totalIps = MainList.size();
        int div = 65;

        int proportions = (int) Math.ceil((double) totalIps / div);

        List<ExecutorService> executorServices = new ArrayList<ExecutorService>();
        for (int i = 0; i < proportions; i++)
            executorServices.add(Executors.newFixedThreadPool(div));

        List<String> ipList = new LinkedList<String>(MainList);
        List<Future<PingResult>> FinalList = new ArrayList<Future<PingResult>>();
        List<PingResult> Result = new ArrayList<PingResult>();

        int startIndex = 0;

        for (ExecutorService server : executorServices) {

            Callable<PingResult> callable = null;
            List<Future<PingResult>> finalList = new ArrayList<Future<PingResult>>();
            Collection<Callable<PingResult>> CallableCollection = new ArrayList<Callable<PingResult>>();

            int count = ipList.size();
            if (count >= div)
                count = div;

            List<String> splitList = MainList.subList(startIndex, startIndex + count);
            startIndex += count;

            for (int i = 0; i < splitList.size(); i++) {
                try {
                    CallableCollection.add(new PingTask(splitList.get(i)));
                    ipList.remove(0);
                }
                catch (Exception ex)
                {
                    Log.v("",ex.getMessage());
                }
            }

            try {
                finalList = server.invokeAll(CallableCollection);
                server.awaitTermination(0, TimeUnit.SECONDS);
                FinalList.addAll(finalList);
                server.shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }//);

        for (int i = 0; i < FinalList.size(); i++) {
            try {
                PingResult r = FinalList.get(i).get(5, TimeUnit.SECONDS);
                if (r.getIsReachable())
                    Result.add(r);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Result;
    }
}