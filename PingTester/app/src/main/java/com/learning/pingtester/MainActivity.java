package com.learning.pingtester;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    float DurationInMilliSeconds = 0;
    TextView mTextField;
    TextView dialogField;

    public static void AskPermissions(Context context) {
        try {
            String[] perms = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE};

            ArrayList<String> strlistperms = new ArrayList<String>();

            for (String perm : perms) {
                if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, perm)) {
                    strlistperms.add(perm);
                }
            }

            String[] permisssions = strlistperms.toArray(new String[strlistperms.size()]);

            if (permisssions != null && permisssions.length > 0) {
                ActivityCompat.requestPermissions((Activity) context, permisssions, 0);
            }
        } catch (Exception ex) {
        }
    }

    private static boolean ExecuteCommand(String command) {
        System.out.println("executeCommand");
        Runtime runtime = Runtime.getRuntime();
        try {
            String cmd = String.format("/system/bin/ping -c 3 %s", command);
            Process mIpAddrProcess = runtime.exec(cmd);
            int mExitValue = mIpAddrProcess.waitFor();
            System.out.println(" mExitValue " + mExitValue);
            if (mExitValue == 0) {
                return true;
            } else {
                return false;
            }
        } catch (InterruptedException ignore) {
            ignore.printStackTrace();
            System.out.println(" Exception:" + ignore);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(" Exception:" + e);
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AskPermissions(this);
        mTextField = (TextView) findViewById(R.id.timeView);
        dialogField = (TextView) findViewById(R.id.dialogView);
        dialogField.setMovementMethod(new ScrollingMovementMethod());
    }

    public void PingClick(View view) {
        view.setEnabled(false);
        dialogField.setText("Ping in process, Please Wait...");
        CountDownTimer countDownTimer = new CountDownTimer(100000, 1) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("Time Spent: " + ((float) (100000 - millisUntilFinished) / 1000) + " seconds");
            }

            public void onFinish() {
            }
        };

        countDownTimer.start();
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                return GetPingResult();
            }

            public void onPostExecute(String message) {
                countDownTimer.cancel();
                dialogField.setText(message);
                view.setEnabled(true);
            }
        }.execute();
    }

    private String GetPingResult() {

        Date beforePingTime = Calendar.getInstance().getTime();
        List<String> MainList = new LinkedList<String>(Constants.GetIPList());
        int totalItems = MainList.size();
        List<PingResult> SuccessfullyPingedServers = ParallelPing.PingServers(MainList);

        String message = "";

        if (SuccessfullyPingedServers.size() == 0)
            message = "Ping Failed";
        else {
            double failPercentage = 100 * ((double) (totalItems - SuccessfullyPingedServers.size()) / totalItems);
            message = "Servers Successfully Pinged: " + SuccessfullyPingedServers.size() + " with 3 tries\n" + failPercentage + "% failed\n\n";
        }

        for (PingResult result : SuccessfullyPingedServers)
            message += result.getIpAddress() + "\u0009  -> (" + result.getResponseTimeString() + ")\n";

        return message;
    }


}
