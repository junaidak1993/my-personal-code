package com.learning.pingtester;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

public class PingTask implements Callable<PingResult> {

    private String ipAddress;

    public PingTask(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public PingResult call() {
        try {
            return PingUtils.ExecutePing(ipAddress, 3);
        } catch (Exception e) {
            e.printStackTrace();
            return new PingResult(ipAddress, false, 0);
        }
    }
}