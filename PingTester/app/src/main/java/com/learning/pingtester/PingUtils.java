package com.learning.pingtester;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PingUtils {
    public static PingResult ExecutePing(String host, int numOfRetries) {

        PingResult pingResult = null;

        try {
            String cmd = "/system/bin/ping -c " + numOfRetries + " -s " + 32 + " -w " + 1 + " " + host;

            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(cmd);

            final InputStream inputStream = process.getInputStream();
            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            while (true) {
                final String string = bufferedReader.readLine();
                if (string == null) {
                    break;
                }

                if (!string.isEmpty() && string.contains(",")) {
                    String[] res = string.split(",");
                    if (res.length > 0) {
                        pingResult = ParsePingResult(host, res);
                        break;
                    }
                }
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        }

        return pingResult == null ? new PingResult(host, false, 0) : pingResult;
    }

//    Sample Response
//    PING google.com (204.228.150.3) 56(84) bytes of data.
//    64 bytes from www.google.com (204.228.150.3): icmp_seq=1 ttl=63 time=0.267 ms
//
//    --- google.com ping statistics ---
//        1 packets transmitted, 1 received, 0% packet loss, time 0ms
//    rtt min/avg/max/mdev = 0.267/0.267/0.267/0.000 ms

    private static PingResult ParsePingResult(String host, String[] stringList) {
        String ip = host;
        long time = 0;
        boolean isReachable = false;
        try {
            int recieved = 0;
            if (stringList.length > 1)
                recieved = Character.getNumericValue(stringList[1].trim().charAt(0));

            isReachable = stringList.length > 3 && recieved > 0;
            if (isReachable)
                time = Integer.parseInt(stringList[3].split("time")[1].toString().split("ms")[0].trim().toString());

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return new PingResult(ip, isReachable, time);
    }


}
