package com.learning.pingtester;

public class PingResult {

    private String IPAddress;
    private int ResultCode;
    private long ResponseTime;
    private boolean IsReachable;

    public PingResult(String ipAddress, int resultCode, long responseTime) {
        this.IPAddress = ipAddress;
        this.ResultCode = resultCode;
        this.IsReachable = resultCode == 0;
        this.ResponseTime = responseTime;
    }

    public PingResult(String ipAddress, boolean isReachable, long responseTime) {
        this.IPAddress = ipAddress;
        this.IsReachable = isReachable;
        this.ResponseTime = responseTime;
    }

    public String getResponseTimeString() {
        return ResponseTime + "ms";
    }

    public long getResponseTime() {
        return ResponseTime;
    }

    public String getIpAddress() {
        return IPAddress;
    }

    public int getResultCode() {
        return ResultCode;
    }

    public boolean getIsReachable() { return IsReachable; }

    public String toString() {
        return "IpAddress :: " + IPAddress + " Result Code : " + ResultCode + " Response Time: " + ResponseTime + "ms";
    }
}