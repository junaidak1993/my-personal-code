package com.learning.learning;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ParallelPing {

    public static List<PingResult> PingServers() {

        String [] IpArray = {"108.61.93.2","96.44.180.30","23.94.11.110","23.95.82.250","173.236.23.2","108.178.63.246","174.34.173.218","23.80.48.206","173.239.28.38","62.233.127.182","83.170.118.3 ","184.105.140.166","94.249.161.5","109.202.96.154","80.67.0.218","72.52.92.30","31.192.105.130","199.59.206.130","185.125.32.194","95.141.47.253","103.14.77.2","83.167.254.128","78.152.44.95","192.253.240.194","154.54.24.222","168.209.1.188","217.64.32.253","91.201.56.130","192.253.251.2","4.69.153.190","185.55.216.253","188.72.116.2","31.210.11.38","128.0.34.62","109.163.232.50","78.153.192.161","79.133.201.132","80.78.69.126","177.154.148.220","119.81.28.170","78.152.57.111","212.103.65.78","212.47.201.191","185.20.96.166","37.187.171.157","192.99.100.57","194.44.88.74","217.113.61.222","119.81.130.170","168.1.1.212","119.81.28.170","103.254.152.213","94.242.199.194","120.89.91.67","161.202.125.20","221.121.137.25","80.255.15.102","199.244.116.30","202.133.244.130","104.216.247.3","80.255.15.102","199.48.68.90","221.121.156.249","38.122.181.114","221.121.135.3","45.74.63.2","45.74.58.2","50.97.230.36","173.193.70.98","67.228.112.250","5.10.97.132","89.149.181.13","37.221.166.2","72.249.199.170","63.218.42.106","168.1.1.212","98.142.101.250","208.43.102.250","159.122.128.84","158.85.65.212","50.23.64.58","162.252.180.13","169.57.128.148","38.122.64.74","94.100.17.251","169.53.79.168","158.205.213.90","172.94.0.2","221.121.155.249","220.196.58.129","154.25.5.198","222.76.221.178","175.41.61.222","104.140.0.122","223.25.240.2","222.112.99.12","154.54.44.105","184.105.223.189","92.220.95.234","172.111.197.2","172.111.195.2","211.95.72.184","211.95.72.184","211.95.72.184","211.95.72.184","89.238.191.38","217.64.113.10","216.156.108.18","80.64.98.227","154.54.58.14","97.107.179.19","38.88.8.22","5.254.107.70","154.24.15.50","83.217.231.94","77.81.98.186","185.104.187.26","138.99.209.1","213.21.192.207","199.71.235.10","193.169.198.80","113.23.230.110","91.218.186.93","37.46.195.66","203.162.155.9","186.177.78.153","103.60.9.42","199.47.199.1","83.143.245.50"};

        int totalIps = IpArray.length;
        ExecutorService executor = Executors.newFixedThreadPool(totalIps);
        List<Future<PingResult>> FinalList = new ArrayList<Future<PingResult>>();
        Callable<PingResult> callable = null;

        Collection<Callable<PingResult>> CallableCollection = new ArrayList<Callable<PingResult>>();
        List<PingResult> Result = new ArrayList<PingResult>();

        for (String ip: IpArray) {
            try {
                CallableCollection.add(new PingTask(ip));
            }
            catch (Exception ex){}
        }

        try {
            FinalList = executor.invokeAll(CallableCollection);
            executor.awaitTermination(0, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();

        for (int i = 0; i < FinalList.size(); i++) {
            try {
                PingResult r = FinalList.get(i).get(5, TimeUnit.SECONDS);
                if (r.getResultCode() == 0)
                    Result.add(r);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Result;
    }
}