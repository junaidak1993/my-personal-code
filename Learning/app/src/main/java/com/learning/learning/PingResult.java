package com.learning.learning;

public class PingResult {

  private String ipAddress;
  private int resultCode;
  private long responseTime;

  public PingResult(String ipAddress, int resultCode, long responseTime) {
    this.ipAddress = ipAddress;
    this.resultCode = resultCode;
    this.responseTime = responseTime;
  }

  public String getResponseTimeString() {return responseTime + "ms"; }

  public long getResponseTime() {return responseTime; }

  public String getIpAddress() {
    return ipAddress;
  }

  public int getResultCode() {
    return resultCode;
  }

  public String toString() {
    return "IpAddress :: "+ ipAddress + " Result Code : "+ resultCode + " Response Time: " + responseTime + "ms";
  }
}