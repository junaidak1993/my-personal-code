package com.learning.learning;

import java.io.OutputStream;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

public class PingTask implements Callable<PingResult> {

  private String ipAddress;

  public PingTask(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  @Override
  public PingResult call() {
    InetAddress inet = null;
    try {
      inet = InetAddress.getByName(ipAddress);
      Date beforePingTime = Calendar.getInstance().getTime();
      int resultCode = inet.isReachable(5000) ? 0 : -1;
      Date afterPingTime = Calendar.getInstance().getTime();
      long pingTime = afterPingTime.getTime() - beforePingTime.getTime();
      return new PingResult(ipAddress, resultCode, pingTime);
    } catch (Exception e) {
      e.printStackTrace();
      return new PingResult(ipAddress, -1, 0);
    }
  }
}