package com.learning.learning;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.learning.learning.databinding.ActivityMainBinding;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter(this);
        TemperatureData temperatureData = new TemperatureData("10");
        binding.setTemp(temperatureData);
        binding.setPresenter(mainActivityPresenter);

        AskPermissions(this);

        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                return GetPingResult();
            }

            public void onPostExecute(String message)
            {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }.execute();


    }

    private String GetPingResult() {

        Date beforePingTime = Calendar.getInstance().getTime();
        List<PingResult> SuccessfullyPingedServers = ParallelPing.PingServers();

        String message = "";

        if (SuccessfullyPingedServers.size() == 0)
            message = "Ping Failed";
        else {
            double failPercentage = 100*((double)(129 - SuccessfullyPingedServers.size())/129);
            Date afterPingTime = Calendar.getInstance().getTime();
            long pingTime = (afterPingTime.getTime() - beforePingTime.getTime()) / 1000;
            message = "Servers Successfully Pinged: " + SuccessfullyPingedServers.size() + " in " + pingTime + " seconds" + "\n" + failPercentage + "% failed";
        }

        return message;
    }

    @Override
    public void showData(TemperatureData temperatureData) {
        String celsius = temperatureData.getCelsius();
        Toast.makeText(this, celsius, Toast.LENGTH_SHORT).show();
    }

    public static void AskPermissions(Context context) {
        try {
            String[] perms = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE};

            ArrayList<String> strlistperms = new ArrayList<String>();

            for (String perm : perms) {
                if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, perm)) {
                    strlistperms.add(perm);
                }
            }

            String[] permisssions = strlistperms.toArray(new String[strlistperms.size()]);

            if (permisssions != null && permisssions.length > 0) {
                ActivityCompat.requestPermissions((Activity) context, permisssions, 0);
            }
        } catch (Exception ex) {
        }
    }
}
