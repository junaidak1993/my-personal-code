package com.learning.learning;

/**
 * Created by Junaid AK on 20-Feb-17.
 */
public class User
{
    public final String firstName;
    public final String lastName;

    public User(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
